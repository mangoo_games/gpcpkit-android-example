package com.greenpanda.gpcpkit_android_example;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by guillaumebargot on 27/02/2018.
 */

public class CrosspromoObject extends LinearLayout {

    private LayoutInflater inflater;
    private LinearLayout layout;
    private ImageView image;
    private TextView name;


    public CrosspromoObject(Context context) {
        super(context);
        inflater = LayoutInflater.from(context);
        init();
    }

    public CrosspromoObject(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflater = LayoutInflater.from(context);
        init();
    }

    public CrosspromoObject(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflater = LayoutInflater.from(context);
        init();
    }

    private void init(){
        inflater.inflate(R.layout.crosspromo_object,this,true);

        image = findViewById(R.id.crosspromo_image);
        name = findViewById(R.id.crosspromo_name);
        layout = findViewById(R.id.crosspromo_layout_inner);

        Typeface typeface = ResourcesCompat.getFont(getContext(), R.font.montserrat_normal);
        name.setTypeface(typeface);
    }

    public void setImage(Bitmap bitmap){
        image.setImageBitmap(bitmap);
    }

    public void setText(String string){
        name.setText(string);
    }

    public void setOnClickListener(OnClickListener listener){
        layout.setOnClickListener(listener);
    }
}

