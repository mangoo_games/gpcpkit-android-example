package com.greenpanda.gpcpkit_android_example;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.greenpanda.gpcpkit.GPCPKit;

import java.util.ArrayList;

/**
 * Created by guillaumebargot on 27/02/2018.
 */

public class NativeRetainedFragment extends Fragment {

    private ArrayList<GPCPKit.GPCPNativeAd> ads;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void setAds(ArrayList<GPCPKit.GPCPNativeAd> ads) {
        this.ads = ads;
    }

    public ArrayList<GPCPKit.GPCPNativeAd> getAds() {
        return ads;
    }
}
