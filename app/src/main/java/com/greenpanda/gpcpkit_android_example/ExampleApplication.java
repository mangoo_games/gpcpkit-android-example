package com.greenpanda.gpcpkit_android_example;

import android.app.Application;

import com.greenpanda.gpcpkit.GPCPKit;
import com.greenpanda.gpcpkit.GPCPKitDelegate;
import com.greenpanda.gpcpkit.GPCPPlacement;
import com.greenpanda.gpcpkit.PlacementElement;

public class ExampleApplication extends Application {
    private GPCPPlacement placement;
    private GPCPPlacement nativePlacement;

    @Override
    public void onCreate() {
        super.onCreate();

        //This is the delegate, you can trigger actions here if you want something to happen.
        GPCPKitDelegate gpDelegate = new GPCPKitDelegate() {
            @Override
            public void GPCPKitWillShow(GPCPPlacement placement) {
                //An ad should be shown
            }

            @Override
            public void GPCPKitDidShow(GPCPPlacement placement, PlacementElement element) {
                //An ad has been shown
            }

            @Override
            public void GPCPKitDidClose(GPCPPlacement placement) {
                //An ad has been closed
            }

            @Override
            public void GPCPKitDidClickOn(GPCPPlacement placement, PlacementElement element) {
                //An ad has been clicked on
            }
        };

        //We use the init with the number of an already working app
        GPCPKit.init(this, 25, gpDelegate);

        //We create the placements using the names that we got from the GreenPanda's backoffice
        placement = new GPCPPlacement("appstart", GPCPPlacement.GPCPPlacementType.FULLSCREEN);
        nativePlacement = new GPCPPlacement("homescreen", GPCPPlacement.GPCPPlacementType.NATIVE);

        //We finally add the placements to the library
        GPCPKit.addPlacement(placement);
        GPCPKit.addPlacement(nativePlacement);
    }

    public GPCPPlacement getPlacement(){
        return placement;
    }

    public GPCPPlacement getNativePlacement(){
        return nativePlacement;
    }
}
