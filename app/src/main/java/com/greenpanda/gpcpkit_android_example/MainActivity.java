package com.greenpanda.gpcpkit_android_example;

import android.content.res.Configuration;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.greenpanda.gpcpkit.GPCPKit;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Button nativeButton;
    private Button interstitialButton;

    private LinearLayout crossPromoLayout;
    private int maximumCrosspromoNumber = 4;

    private Runnable initCrosspromoRunnable = null;
    private ArrayList<GPCPKit.GPCPNativeAd> ads = null;

    private static final String TAG_RETAINED_FRAGMENT = "NativeRetainedFragment";

    private NativeRetainedFragment mRetainedFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_LANDSCAPE){
            maximumCrosspromoNumber = 3;
        }

        FragmentManager fm = getSupportFragmentManager();
        mRetainedFragment = (NativeRetainedFragment) fm.findFragmentByTag(TAG_RETAINED_FRAGMENT);

        if (mRetainedFragment == null) {
            mRetainedFragment = new NativeRetainedFragment();
            fm.beginTransaction().add(mRetainedFragment, TAG_RETAINED_FRAGMENT).commit();
        }else{
            ads = mRetainedFragment.getAds();
        }

        initCrosspromoRunnable = new Runnable() {
            @Override
            public void run() {
                addNativesToLayout();
            }
        };

        nativeButton = findViewById(R.id.nativeButton);
        nativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initCrossPromo();
            }
        });

        interstitialButton = findViewById(R.id.interstitialButton);


        interstitialButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInterstitial();
            }
        });

        initCrossPromo();
    }


    private void showInterstitial(){
        GPCPKit.show(this,((ExampleApplication)getApplication()).getPlacement());
    }

    private void initCrossPromo(){
        crossPromoLayout = findViewById(R.id.crosspromo_layout);

        if(ads==null) {
            GPCPKit.nativeAd(4, ((ExampleApplication) getApplication()).getNativePlacement(),
                    new GPCPKit.GPCPNativeDelegate() {
                        @Override
                        public void onEverythingDownloaded(ArrayList<GPCPKit.GPCPNativeAd> nativeAds) {
                            if (nativeAds != null) {
                                ads = nativeAds;
                                mRetainedFragment.setAds(ads);
                                initCrosspromoRunnable.run();
                            }
                        }
                    });
        }else{
            addNativesToLayout();
        }
    }


    private void addNativesToLayout(){
        for (int i = 0; i < maximumCrosspromoNumber && (crossPromoLayout.getChildCount() - (crossPromoLayout.getChildCount()/2)) < maximumCrosspromoNumber; i++) {
            final GPCPKit.GPCPNativeAd nativeAd = ads.get(i);

            CrosspromoObject object = new CrosspromoObject(getApplicationContext());
            object.setImage(nativeAd.icon);
            object.setText(nativeAd.title);
            object.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    nativeAd.didClickElement(MainActivity.this);
                }
            });
            if (crossPromoLayout.getChildCount() > 0) {
                //The space between the icons
                View inBetweenSpace = new View(getApplicationContext());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(8, ViewGroup.LayoutParams.MATCH_PARENT);
                inBetweenSpace.setLayoutParams(params);
                crossPromoLayout.addView(inBetweenSpace);
            }
            crossPromoLayout.addView(object);
        }
    }
}
