# Library Integration
You can integrate GPCPKit to your app by including it into your dependencies in the application gradle file. The library is available on both maven and jcentral so you shouldn't have to modify your repositories.

Either way, your "Project" gradle file should contain one of those in the "repositories" category:

    repositories {
	    jcenter()
	    mavenCentral()
    }
If you have one of those, then everything is good. If not, or if you don't have a repositories category at all, we recommend you to add jcenter() as it's Google's preferred one.

Once this is done, simply add this line in the dependencies part:

    compile 'com.greenpandagames.publishing:gpcpkit:0.5.3'

## Setup your Application

First, if not done, define your application name in the manifest:

    <application
        android:name="MyApplication"
        android:label="@string/app_name"
        android:theme="@style/theme" >
        <activity
            android:name="MyActivity"
    ...

Then, create an Application java file and initialize GPCPKit in there.

    public class MyApplication extends Application {

	    public void onCreate() {
	        super.onCreate();
	        GPCPKit.init(this, 000, null);
	    }
    }

>**Note**: The 000 above is to replace with the Crosspromo Id that was given to you.

The null part is where the Delegate should be. You can use the delegate if you need to know when ads are shown, clicked on, closed, etc. Here is an example of how to use it. This code also initializes the GPCPKit library.

    GPCPKitDelegate gpDelegate = new GPCPKitDelegate() {
            @Override
            public void GPCPKitWillShow(GPCPPlacement placement) {
				//An ad should be shown
            }

            @Override
            public void GPCPKitDidShow(GPCPPlacement placement, PlacementElement element) {
                //An ad has been shown
            }

            @Override
            public void GPCPKitDidClose(GPCPPlacement placement) {
				//An ad has been closed
            }

            @Override
            public void GPCPKitDidClickOn(GPCPPlacement placement, PlacementElement element) {
               //An ad has been clicked on
            }
        };

        GPCPKit.init(this,000,gpDelegate);

## Register your placements

The next step is to create GPCPPlacements that we will use to show ads in different contexts. To do so, please write this line of code, still inside the onCreate of your Application class, to create each placement you will have in your app: 

    placement = new GPCPPlacement("appstart", GPCPPlacement.GPCPPlacementType.FULLSCREEN);
    nativePlacement = new GPCPPlacement("homescreen", GPCPPlacement.GPCPPlacementType.NATIVE);

>**Note**: The NATIVE and FULLSCREEN parameters determine which type of ad is gonna be shown in this placement.

After you created your placements, you should add them to GPCPKit like such:

    GPCPKit.addPlacement(placement);
    GPCPKit.addPlacement(nativePlacement);

Once this is done, you should have successfully setup the library. Here is a final example of how the onCreate of your Application class should look (without delegate):

    public class MyApplication extends Application {
    
		private GPCPPlacement placement;
		private GPCPPlacement nativePlacement;
		
	    public void onCreate() {
	        super.onCreate();
	        GPCPKit.init(this, 000, null);
	        placement = new GPCPPlacement("appstart", GPCPPlacement.GPCPPlacementType.FULLSCREEN);
		    nativePlacement = new GPCPPlacement("homescreen", GPCPPlacement.GPCPPlacementType.NATIVE);
		    GPCPKit.addPlacement(placement);
		    GPCPKit.addPlacement(nativePlacement);
	    }
    }

## Show an interstitial

To show an interstitial, simply write this line of code where you want it to be shown (on the UI Thread) :

    GPCPKit.show(this,placement);

>**Note 1**: The placement here is one of the placement you created before. We recommend you store the placement in the Application java file so you can get them from everywhere using the Application Context.

>**Note 2**: "this" in this case should be an Activity. We need this in order to launch our Ad Activity but also to have a style that matches your app's.

The function returns a boolean that indicates if the ad has indeed been shown or not.

## Show a native ad

When you call for a native ad, we return you a GPCPNativeAd object that contains the following parameters:

- id: an int containing the id of the placement
- title: a String that contains the name of the app
- bitmap: a Bitmap of the icon of the app 

It also contains a function called didClickElement:

    public void didClickElement(Activity currentActivity)
You should call this function when the user clicks on your personalized ad to download the app displayed.

To retrieve native ads, you have to call this function:

    GPCPKit.nativeAd(1,nativePlacement, gpcpNativeDelegate)
>Note: The number 1 above indicates that we only want one native ad. The native placement is one that we stored in the Application.


>Note: The gpcpNativeDelegate parameter is a GPCPNativeDelegate object that contains a function called onEverythingDownloaded(ArrayList<GPCPNativeAd> ads). To cover the possibility of us wanting to show more than one native ad at the same time, the onEverythingDownloaded function gives you an array of native ads.

This function does not returns anything but you get to recover the GPCPNativeAd objects in the function included in the GPCPNativeDelegate. This accounts for the times when graphic files of native ads are not loaded and we have to wait for them to load before returning native ad objects. 

Here is an example of how to use it to show an ad containing an ImageView for the icon and a separate "Download" Button:

    ImageView image = findView(...);
    Button dlButton = findView(...);
    ArrayList<GPCPKit.GPCPNativeAd> ads = GPCPKit.nativeAd(1,nativePlacement, new GPCPKit.GPCPNativeDelegate() {
            @Override
            public void onEverythingDownloaded(ArrayList<GPCPKit.GPCPNativeAd> ads) {
                if(ads!=null && ads.size()>0) {
	                final GPCPKit.GPCPNativeAd nativeAd = ads.get(0);
                    image.setImageBitmap(nativeAd.icon);
                    dlButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            nativeAd.didClickElement(MyActivity.this);
                        }
                    });
                }
            });
        

On the first launch, the callback that gets the native icons can be disfunctionnal if your app can rotate from portrait to landscape. You should take care of this problem in your home screen. To see an example, check this repo's solution in MainActivity.